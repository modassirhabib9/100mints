import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:user_app/api_services.dart';
import 'package:user_app/banner_model22.dart';
import 'package:user_app/data/model/response/base/api_response.dart';
import 'view/screen/wishlist/Extra/latitude_longitude.dart';

class Check extends StatefulWidget {
  @override
  _CheckState createState() => _CheckState();
}

class _CheckState extends State<Check> {
  List<Latitude_longitude> list = [];
  bool listcheck = false;
  getDataHere() async {
    print('check screen');
    list = await ApiServicesClass().getData(list);
    print(list.length);
    print('done');
    setState(() {
      listcheck = true;
    });
    print(list[0].id);
    print(list[0].name);
    print(list[0].addedBy);
    print(list[1].thumbnail);
  }

  @override
  void initState() {
    super.initState();
    getDataHere();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('check'),
        ),
        body: listcheck
            ? ListView.builder(
                itemCount: list.length,
                itemBuilder: (context, i) {
                  return Column(
                    children: [
                      Text(
                        list[i].id.toString(),
                      ),
                      Text(
                        list[i].name.toString(),
                      ),
                      Text(
                        list[i].userId.toString(),
                      ),
                      Text(
                        list[i].addedBy.toString(),
                      ),
                    ],
                  );
                })
            : Text('Loading'));
  }
}
