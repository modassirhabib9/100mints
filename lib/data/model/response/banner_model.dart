class BannerModel {
  ///////////////////////
  /*
  /// id : 23
/// photo : "2021-11-10-618b652f8729f.png"
/// banner_type : "Main Banner"
/// published : 1
/// category : "1"
/// created_at : "2021-11-10 12:22:39"
/// updated_at : "2021-11-10 12:23:15"
/// url : "https://khybercoded.pk"

class Api {
  Api({
    int id,
    String photo,
    String bannerType,
    int published,
    String category,
    String createdAt,
    String updatedAt,
    String url,
  }) {
    _id = id;
    _photo = photo;
    _bannerType = bannerType;
    _published = published;
    _category = category;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _url = url;
  }

  Api.fromJson(dynamic json) {
    _id = json['id'];
    _photo = json['photo'];
    _bannerType = json['banner_type'];
    _published = json['published'];
    _category = json['category'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _url = json['url'];
  }
  int _id;
  String _photo;
  String _bannerType;
  int _published;
  String _category;
  String _createdAt;
  String _updatedAt;
  String _url;

  int get id => _id;
  String get photo => _photo;
  String get bannerType => _bannerType;
  int get published => _published;
  String get category => _category;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  String get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['photo'] = _photo;
    map['banner_type'] = _bannerType;
    map['published'] = _published;
    map['category'] = _category;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['url'] = _url;
    return map;
  }
}

  */
  ////////////////////////
  int _id;
  String _photo;
  String _bannerType;
  int _published;
  String category;
  String _createdAt;
  String _updatedAt;
  String _url;

  BannerModel(
      {int id,
      String photo,
      String bannerType,
      int published,
      String category,
      String createdAt,
      String updatedAt,
      String url}) {
    this._id = id;
    this._photo = photo;
    this._bannerType = bannerType;
    this._published = published;
    this._createdAt = createdAt;
    this.category = category;
    this._updatedAt = updatedAt;
    this._url = url;
  }

  int get id => _id;

  String get photo => _photo;

  String get bannerType => _bannerType;

  int get published => _published;

  String get createdAt => _createdAt;
  String get gategory => category;

  String get updatedAt => _updatedAt;

  String get url => _url;

  BannerModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _photo = json['photo'];
    _bannerType = json['banner_type'];
    _published = json['published'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    category = json['category'];
    _url = json['url'];
    print("Ok " + category);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['photo'] = this._photo;
    data['banner_type'] = this._bannerType;
    data['published'] = this._published;
    data['created_at'] = this._createdAt;
    data['category'] = category;
    data['updated_at'] = this._updatedAt;
    data['url'] = this._url;
    return data;
  }
}
