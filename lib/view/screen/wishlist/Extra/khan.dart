import 'dart:convert';

import 'package:http/http.dart' as http;

class Services {
  Future<dynamic> addToSql(String name, String roll, String gpa) async {
    http.Response response = await http
        .post(Uri.parse('http://192.168.43.88/myapp/data.php'), body: {
      "name": name,
      "roll": roll,
      "gpa": gpa,
    });

    if (response.statusCode == 200) {
      print(response.body);
      return response.body;
    }
  }

  Future<dynamic> getData() async {
    http.Response response =
        await http.get(Uri.parse('http://192.168.43.88/myapp/getData.php'));
    if (response.statusCode == 200) {
      print(response.body);
      var data = response.body;
      var decodedData = jsonDecode(data);
      return decodedData;
    }
  }
}
